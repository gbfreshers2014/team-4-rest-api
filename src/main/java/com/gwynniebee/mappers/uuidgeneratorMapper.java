package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class uuidgeneratorMapper implements ResultSetMapper<String>{

    @Override
    public String map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        // TODO Auto-generated method stub
        return r.getString("uuid");
    }
    

}
