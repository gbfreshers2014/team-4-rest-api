package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.deleteresponseobject;

public class deleteMapper implements ResultSetMapper<deleteresponseobject> {
    @Override
    public deleteresponseobject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
      
    	deleteresponseobject lro=new deleteresponseobject();      
      
      if(r.first()==true)
      {
          lro.msg="successful";
      }
      else
    	  lro.msg="unsuccessful";
      
      return lro;
    }

}
