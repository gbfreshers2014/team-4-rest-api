package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.updateresponseobject;

public class updateMapper implements ResultSetMapper<updateresponseobject> {
    @Override
    public updateresponseobject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
      
    	updateresponseobject lro=new updateresponseobject();      
      
      if(r.first()==true)
      {
          lro.msg="successful";
      }
      else
    	  lro.msg="unsuccessful";
      
      return lro;
    }

}
