package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.loginObject;
import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.backoffice.restlet.resources.loginHandler;
import com.gwynniebee.dao.loginInternaldao;
import com.gwynniebee.dao.logindao;

public class loginEntityManager {

    public loginresponseobject authenticate(loginObject login_inst) throws ClassNotFoundException {
        Handle h=BaseEntityManager.getDBIh();
        logindao dao=null;
        loginInternaldao idao=null;
        h.begin();
        idao=h.attach(loginInternaldao.class);
        dao = h.attach(logindao.class);        
        String uuid= idao.fetchuuid(login_inst.emailid);
       loginresponseobject tobereturned=dao.authenticateuserandspecifyrole(uuid, login_inst.password);                
       h.commit();
       h.close();
       return tobereturned;     
    }

}
