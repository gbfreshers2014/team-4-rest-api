package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;

import org.skife.jdbi.v2.Handle;

import com.gwynniebee.dao.attendanceuploaddao;
import com.gwynniebee.dao.searchdao;
import com.gwynniebee.dao.uuidgeneratordao;

public class attendanceEntityManager {
    
    
    public int updateAttendance(HashSet<String> uuid_set) throws Exception
    {
    Handle h=BaseEntityManager.getDBIh();
    uuidgeneratordao dao=h.attach(uuidgeneratordao.class);
    attendanceuploaddao atddao=h.attach(attendanceuploaddao.class);
    ArrayList<String> uuidlist=(ArrayList<String>) dao.generateUUID();
    java.util.Date utildate = new java.util.Date();
    Date date=new java.sql.Date(utildate.getTime());
    
    for(String uuid:uuidlist)
    {
        if(uuid_set.contains(uuid))
        {
            atddao.updateattendance(uuid, "absent", date);
        }
        else
            atddao.updateattendance(uuid, "present", date);
    }
    
    return 0;
    
    }
}
