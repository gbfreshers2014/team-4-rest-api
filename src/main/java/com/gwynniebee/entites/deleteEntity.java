package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.deleteObject;
import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.dao.deletedao;

public class deleteEntity {

	public deleteresponseobject authenticate(deleteObject delete_inst) throws ClassNotFoundException {
	        Handle h=BaseEntityManager.getDBIh();
	        deletedao dao=null;
	        h.begin();
	        dao = h.attach(deletedao.class);        
	       deleteresponseobject tobereturned =new deleteresponseobject();
	       tobereturned.msg=dao.authenticateadminandchangeempstatus("b")+"";                
	       h.commit();
	       h.close();
	       return tobereturned;     
	    }

}
