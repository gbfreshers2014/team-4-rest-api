package com.gwynniebee.dao;

import java.sql.Date;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

public interface attendanceuploaddao extends Transactional<attendanceuploaddao>{
    @SqlUpdate("insert into attendance_register values(:uuid,:date,:status,null,null,null,null)")
    int updateattendance(@Bind("uuid")String uuid,@Bind("status")String status,@Bind("date") Date date);
}
