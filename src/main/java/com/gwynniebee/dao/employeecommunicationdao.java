package com.gwynniebee.dao;

import java.util.ArrayList;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.address_details;
import com.gwynniebee.backoffice.objects.communication_details;
import com.gwynniebee.mappers.employeecommunincationMapper;

@RegisterMapper(employeecommunincationMapper.class)
public interface employeecommunicationdao extends Transactional<employeecommunicationdao> {

    @SqlQuery("select * from employee_communication_details where uuid=:uuid")
    List<communication_details> sqlquerycommunication(@Bind("uuid") String uuid);
}
