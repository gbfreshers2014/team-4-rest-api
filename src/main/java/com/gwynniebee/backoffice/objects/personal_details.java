package com.gwynniebee.backoffice.objects;

public class personal_details {
    public personal_details(String uuid, String fname, String lname, String emaiID) {
        firstName=fname;
        lastName=lname;
        this.email=emaiID;
        uuid=uuid;
    }
    public String firstName;
    public String lastName;
    public String email;
    public String uuid;
    public String bloodGroup;
    public String employmentStatus;
    public String designation;
    
}
