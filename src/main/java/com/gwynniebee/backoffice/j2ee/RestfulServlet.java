/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.backoffice.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.backoffice.restlet.rest_api;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<rest_api> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new rest_api());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(rest_api app) {
        super(app);
    }
}