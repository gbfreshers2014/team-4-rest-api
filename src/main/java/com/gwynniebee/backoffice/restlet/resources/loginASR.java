package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.loginObject;
import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.entites.loginEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class loginASR extends AbstractServerResource{

    private static final Logger LOG = LoggerFactory.getLogger(loginASR.class);
    @Post
    public loginHandler handle_login(loginObject login_inst) throws JsonParseException, IOException, ClassNotFoundException
    {
      LOG.info("request "+ login_inst.toString());
      ResponseStatus status=new ResponseStatus();
      loginEntityManager loginentityinstance=new loginEntityManager();
      loginHandler loginhandlerinstance=new loginHandler();
      loginresponseobject lro=loginentityinstance.authenticate(login_inst);
      if(lro==null)
      {
          loginhandlerinstance.role="";
          loginhandlerinstance.UUID="";
          status.setCode(ResponseStatus.ERR_CODE_INVALID_REQ);
          status.setMessage(ResponseStatus.MESSAGE_RESOURCE_NOT_FOUND);
          loginhandlerinstance.setStatus(status);
        }
      else
      {
          loginhandlerinstance.role=lro.role;
          loginhandlerinstance.UUID=lro.UUID;
          status.setCode(ResponseStatus.CODE_SUCCESS);
          status.setMessage(ResponseStatus.MESSAGE_SUCCESS);
          loginhandlerinstance.setStatus(status);
      }
      return loginhandlerinstance;
    }
}
