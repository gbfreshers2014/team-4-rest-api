package com.gwynniebee.backoffice.restlet.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.lang.text.StrBuilder;
import org.joda.time.DateTime;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.FileDetails;
import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.cloudio.CloudAccessClient;
import com.gwynniebee.cloudio.CloudAccessClientFactory;
import com.gwynniebee.cloudio.Entry;
import com.gwynniebee.cloudio.SaveOptions;
import com.gwynniebee.entites.attendanceEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class attendanceUploadASR extends AbstractServerResource{
    byte[] uploadBytes;

    private static final Logger LOG = LoggerFactory.getLogger(loginASR.class);
    @Post("multipart")
    public ResponseStatus uploadAttendance(Representation entity) throws Exception
    {
        
        CloudAccessClient s3client = CloudAccessClientFactory.getS3CloudAccessClient("_dev_store");
        Entry entry=s3client.createNewEntry("gbfreshers", 0, "attendance_"+new Date());
        SaveOptions saveOption = new SaveOptions().withPublicAccess(true);
        Map<String,String> rv=fileuploader();
        s3client.save(entry,".csv",rv.get("attendance"),saveOption);
        String str=stringbuilded();
        HashSet<String> absent=new HashSet<String>();
        for (String it : str.split(","))
        {
            LOG.debug(it.trim() );
            absent.add(it.trim());
        }
        attendanceEntityManager aer=new attendanceEntityManager();
        aer.updateAttendance(absent);
        return getRespStatus();
   }
   Map<String, String> fileuploader() throws Exception
    {
        Map<String,String> rv=new HashMap<String,String>();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(1000240);
        RestletFileUpload upload = new RestletFileUpload(factory);
        List<FileItem> items=upload.parseRepresentation(getRequestEntity());
//        items = upload.parseRequest(this.getRequest());
        uploadBytes = null;
        if (items != null) {
            for (FileItem item : items) {
                
                uploadBytes = item.get();
                LOG.debug(new String(uploadBytes));
            //    if (item.getFieldName().equals("attendance.csv"))
                    rv.put("attendance", new String(uploadBytes));
            }
        }
        else {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.append("Invalid Media Type: ");
                sbMessage.append(this.getRequestEntity().getMediaType());
                throw new Exception(sbMessage.toString());
              }
        
        
        return rv;
    }
   String stringbuilded()
   {

       String str=new String(uploadBytes);
       LOG.debug(str);
       return str;
       /*
       StringBuilder strbuild=new StringBuilder();
       
       for(byte b : )
       {
           Byte b_b=b;
           b_b.toString();
           LOG.debug(String.valueOf(b));
           LOG.debug("-"+String.valueOf(b_b));
           strbuild.append(String.valueOf(b));
       }
       return strbuild.toString();
       */
   }
}