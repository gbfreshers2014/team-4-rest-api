package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Delete;

import com.gwynniebee.backoffice.objects.deleteObject;
import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.entites.deleteEntity;

import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class deleteASR extends AbstractServerResource{
	
	public String tempUuid = "";
	@Override
    protected void doInit() {
        super.doInit();
        this.tempUuid = (String) this.getRequestAttributes().get("UUID");
       }
    
	@Delete
	public deletehandler handle_delete(deleteObject delete_inst) throws JsonParseException, IOException, ClassNotFoundException
    {
      ResponseStatus status=new ResponseStatus();
      deleteEntity deleteentityinstance=new deleteEntity();
      
      deletehandler deletehandlerinstance=new deletehandler();
      deleteresponseobject lro=deleteentityinstance.authenticate(delete_inst);
      
      if(lro==null)
      {
          deletehandlerinstance.msg="error";
          status.setCode(ResponseStatus.ERR_CODE_INVALID_REQ);
          status.setMessage(ResponseStatus.MESSAGE_RESOURCE_NOT_FOUND);
          deletehandlerinstance.setStatus(status);
        }
      else
      {
          deletehandlerinstance.msg=lro.msg;
          status.setCode(ResponseStatus.CODE_SUCCESS);
          status.setMessage(ResponseStatus.MESSAGE_SUCCESS);
          deletehandlerinstance.setStatus(status);
      }
      return deletehandlerinstance;
    }
}
